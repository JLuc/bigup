<?php

/**
 * SPIP, Système de publication pour l'internet
 *
 * Copyright © avec tendresse depuis 2001
 * Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James
 *
 * Ce programme est un logiciel libre distribué sous licence GNU/GPL.
 */

namespace Spip\Core\Tests;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Spip\Bigup\Files;


class ExtraireSousCheminsFichiers extends TestCase {
	public static function setUpBeforeClass(): void {
		find_in_path('./inc/filtres.php', '', true);
	}

	public static function providerErrors() {
		$cases[] = [
			[ 'une_erreur_random'],
			[ 'tons' => [ 0 => [ 'sous' => [ 'la' => [ 'pluie' => [ '0' => 'une_erreur_random']]]]]],
		];
		$cases[] = [
			[
				'une_erreur_random',
				'une_erreur_pouet',
				'un_autre_cas_pas_facile',
			],
			[
				'tons' => [ 0 => [ 'sous' => [ 'la' => [ 'pluie' => [ '0' => 'une_erreur_random']]]]],
				'pouet' => 'une_erreur_pouet',
				'autre' => ['cas' => [ 1 => [ 'avec' => [ 2 => ['pièges' => 'un_autre_cas_pas_facile']]]]],
			]
		];

		return $cases;
	}

	#[DataProvider('providerErrors')]
	public function testExtraireSousCheminsFichiersPHPs($expected, $data) {
		$chemins = Files::extraire_sous_chemins_fichiers($data);
		$res = [];
		foreach ($chemins['phps'] as $chemin) {
			$var = '$data' . $chemin;
			eval("\$error = $var;");
			$res[] = $error;
		}
		$this->assertEquals($expected, $res);
	}
	#[DataProvider('providerErrors')]
	public function testExtraireSousCheminsFichiersUnset($expected, $data) {
		$dataphps = json_decode(json_encode($data), true);
		$datatablevaleurs = json_decode(json_encode($data), true);
		$chemins = Files::extraire_sous_chemins_fichiers($data);
		foreach ($chemins['phps'] as $chemin) {
			$var = '$dataphps' . $chemin;
			eval("\$error = $var;unset($var);");
		}
		foreach ($chemins['table_valeurs'] as $chemin) {
			Files::unset_table_valeur($datatablevaleurs, $chemin);
		}
		$this->assertEquals($dataphps, $datatablevaleurs);
	}

	#[DataProvider('providerErrors')]
	public function testExtraireSousCheminsFichiersTableValeurs($expected, $data) {
		$chemins = Files::extraire_sous_chemins_fichiers($data);
		$res = [];
		foreach ($chemins['table_valeurs'] as $chemin) {
			$res[] = \table_valeur($data, $chemin);
		}
		$this->assertEquals($expected, $res);
	}
}
