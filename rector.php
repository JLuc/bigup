<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
	$rectorConfig->paths([
		__DIR__ . '/action',
		__DIR__ . '/balise',
		__DIR__ . '/formulaires',
		__DIR__ . '/genie',
		__DIR__ . '/inc',
		__DIR__ . '/bigup_administration.php',
		__DIR__ . '/bigup_fonction.php',
		__DIR__ . '/bigup_pipelines.php',
	]);

	$rectorConfig->sets([
		LevelSetList::UP_TO_PHP_81
	]);
};
