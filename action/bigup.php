<?php

use Spip\Bigup\Repondre;

/**
 * Assez tôt on vérifie si on demande à tester la présence d'un morceau de fichier uploadé
 * ou si on demande à envoyer un morceau de fichier.
 *
 * Flow vérifie évidement que l'accès est accrédité !
 */
function action_bigup_dist(): never {
	$bigup = Repondre::depuisRequest();
	$bigup->repondre();

	exit;
}
